const webpack = require('webpack');
const autoprefixer = require('autoprefixer');

const helpers = require('./helpers');

const ENV = process.env.ENV = process.env.NODE_ENV = 'test';

const sassLoaderOptions = {
    includePaths: [
        helpers.root('node_modules'),
        helpers.root('bower_components')
    ]
};
const postCssLoaderOptions = {
    plugins: () => [
        autoprefixer({
            browsers: ['last 2 version']
        })
    ]
};

module.exports = function (options) {
    return {

        /**
         * Source map for Karma from the help of karma-sourcemap-loader &  karma-webpack
         *
         * See: https://webpack.js.org/configuration/devtool/
         */
        devtool: 'inline-source-map',

        /**
         * Entry
         * Reference: https://webpack.js.org/configuration/entry-context/#entry
         */
        // entry: {},
        // revert to setting empty object when https://github.com/webpack-contrib/karma-webpack/pull/221/commits/1f8b5132ceab70c9d5df1f3faa1bbf73da3647e2 is merged

        /**
         * Resolve
         * Reference: https://webpack.js.org/configuration/resolve/
         */
        resolve: {
            extensions: ['.ts', '.js', '.json', '.css', '.scss', '.html'],
            modules: [
                helpers.root('node_modules'),
                helpers.root('bower_components'),
                helpers.root('src')
            ]
        },

        /**
         * Loaders
         * Reference: https://webpack.js.org/configuration/module/#module-rules
         * Available loaders: https://webpack.js.org/loaders/
         */
        module: {
            rules: [
                // tslint support
                {
                    test: /\.ts$/,
                    enforce: 'pre',
                    loader: 'tslint-loader',
                    include: [
                        helpers.root('src')
                    ],
                    options: {
                        emitErrors: false,
                        failOnHint: false
                    }
                },
                // Support for .ts files.
                // Replace templateUrls & styleUrls with inline templates and styles.
                {
                    test: /\.ts$/,
                    use: [
                        {
                            loader: 'awesome-typescript-loader',
                            options: {
                                configFileName: 'tsconfig.test.json',
                                inlineSourceMap: true,
                                sourceMap: false
                            }
                        },
                        {
                            loader: 'angular2-template-loader'
                        }
                    ],
                    exclude: [
                        /\.(e2e)\.ts$/
                    ]
                },

                // all css required in src/app files will be merged in js files
                {
                    test: /\.css$/,
                    include: helpers.root('src'),
                    use: [
                        {
                            loader: 'raw-loader'
                        },
                        {
                            loader: 'postcss-loader',
                            options: postCssLoaderOptions
                        }
                    ]
                },

                // all scss/sass required in src/app files will be merged in js files
                {
                    test: /\.(scss|sass)$/,
                    include: helpers.root('src'),
                    use: [
                        {
                            loader: 'raw-loader',
                        },
                        {
                            loader: 'sass-loader',
                            options: sassLoaderOptions
                        }
                    ]
                },

                // support for .html as raw text
                {
                    test: /\.html$/,
                    loader: 'raw-loader'
                },

                /**
                 * Map coverage on typescript files.
                 *
                 * See: https://github.com/deepsweet/istanbul-instrumenter-loader
                 */
                {
                    test: /\.ts$/,
                    enforce: 'post',
                    loader: 'istanbul-instrumenter-loader',
                    include: helpers.root('src'),
                    exclude: [
                        /\.(e2e|spec)\.ts$/,
                        /node_modules/
                    ]
                }
            ]
        },

        /**
         * Plugins
         * Reference: https://webpack.js.org/configuration/plugins/
         * Available plugins: https://webpack.js.org/plugins/
         */
        plugins: [
            /**
             * Define free variables
             * Reference: https://webpack.js.org/plugins/define-plugin/
             */
            new webpack.DefinePlugin({
                'ENV': JSON.stringify(ENV),
                'process.env': {
                    'ENV': JSON.stringify(ENV),
                    'NODE_ENV': JSON.stringify(ENV)
                }
            }),

            /**
             *  Workaround needed for angular 2 https://github.com/angular/angular/issues/11580
             */
            new webpack.ContextReplacementPlugin(
                // The (\\|\/) piece accounts for path separators in *nix and Windows
                /angular(\\|\/)core(\\|\/)@angular/,
                helpers.root('./src') // location of your src
            )
        ],

        /*
         * Include polyfills or mocks for various node stuff
         * Description: Node configuration
         *
         * See: https://webpack.github.io/docs/configuration.html#node
         */
        node: {
            dns: 'mock', // fetch required setting
            net: 'mock', // fetch required setting
            global: true,
            process: true,
            module: false,
            clearImmediate: false,
            setImmediate: false
        }
    };
};
