# Module store

Factory to bundle angular modules

## Usage
```
npm install
```

Build your angular module in the src folder.
Restriction: put everything (components and module) in 1 file

```
npm run bundle
```
This bundles your module into the file dist/bundle.js

To serve your files:
```
npm start
```

## Settings

bundle module definition: UMD
server-port: 3002