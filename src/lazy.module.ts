import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-lazy-component',
  template: `
    <h2>Lazy component loaded! Hoera!</h2>
  `,
  styles: ['h2 {border: 1px solid #ccc;border-radius: 4px;}']
})
export class LazyComponent {}

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LazyComponent
  ],
  exports: [
    LazyComponent
  ]
})
export class LazyModule {}
